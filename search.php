<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reality</title>
</head>
<body>
<?php
require('data.php');
include 'data.php';

function search_in_ads($array, $string)
{
    global $mesta, $kategorie;
    $foundAds = [];
    $index = 0;
    foreach ($array as $key => $item) {
        foreach($item as $valueKey => $value){
            $searchPosition = null;
            switch ($valueKey) {
                case "cena":
                case "info":
                case "obrazky":
                    break;
                case "mesto":
                    $searchPosition = $mesta[$value];
                    break;
                case "kategorie":
                    $searchPosition = $kategorie[$value];
                    break;
                default:
                    $searchPosition = $value;
                    break;
            }
            if(strpos(strtolower($searchPosition), strtolower($string)) !== false){
                $foundAds[$index] = $item;
                $index++;
                break;
            }
        }

    }
    return $foundAds;
}

?>
<header>
    <a href="index.php"><img src="images/home.jpg" alt=""></a>
    <a href="index.php"><h1>SSSVT reality</h1></a>

    <div id="cities">
        <?php
        foreach ($mesta as $index => $mesto):
            ?>
            <a href="selection.php?mesto=<?= $index ?>"><?= $mesto ?></a>
        <?php endforeach; ?>
    </div>
    <div id="categories">
        <?php
        foreach ($kategorie as $index => $katka):
            ?>
            <a href="selection.php?kategorie=<?= $index ?>"><?= $katka ?></a>
        <?php endforeach; ?>
    </div>
    <div id="searchform">
        <form method="get" action="search.php">
            <input type="text" name="searchstring" value="<?=$_GET['searchstring']?>"/>
            <input type="submit" value="Hledat"/>
        </form>
    </div>

</header>


<div class="content">
    <div id="contentheader">
        <h2>Výběr z inzerátů</h2>
        <h2>Vyhledávání: <?= $_GET['searchstring'] ?></h2>
    </div>
    <div class="inzeraty">
        <?php
        if($_GET['searchstring'] !== '')
            foreach (search_in_ads($nemovitosti, $_GET['searchstring']) as $nemovitost):
            ?>
            <div class="nemovitost">
                <div class="nemovitostheader">
                    <a href="detail.php?id=<?= array_search($nemovitost, $nemovitosti) ?>">
                        <h1><?= $nemovitost['nazev'] ?></h1></a>
                    <div class="clickables">
                        <a href="selection.php?mesto=<?= $nemovitost['mesto'] ?>"
                           class="city"><?= $mesta[$nemovitost['mesto']] ?> </a>
                        <a href="selection.php?kategorie=<?= $nemovitost['kategorie'] ?>"
                           class="category"><?= $kategorie[$nemovitost['kategorie']] ?></a>
                    </div>
                </div>
                <div id="location"><?= $nemovitost['misto'] ?></div>
                <div class="descriptionline">
                    <div class="description"><?= $nemovitost['popis'] ?></div>
                    <div class="foto"><img src="<?= $nemovitost['obrazky'][0] ?>" alt=""></div>
                </div>
                <div class="price"><?= $nemovitost['cena'] ?></div>
            </div>
        <?php endforeach; ?>
    </div>

</div>

</body>
</html>