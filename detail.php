<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Detail</title>
</head>
<body>
<header>
    <a href="index.php"><img src="images/home.jpg" alt=""></a>
    <a href="index.php"><h1>SSSVT reality</h1></a>
</header>
<?php
require('data.php');
include 'data.php';

$nemovitost = $nemovitosti[$_GET['id']];
?>
<div id="detailcontent">
    <div id="imagerow">
        <div id="images">
            <div class="mainphoto"><img src="<?=$nemovitost['obrazky'][0]?>" alt=""></div>

            <div class="imageflex">
            <?php for($i = 1; $i < count($nemovitost['obrazky']); $i++):?>
            <div class="otherphoto"><img src="<?=$nemovitost['obrazky'][$i]?>" alt=""></div>
            <?php endfor; ?>
            </div>

        </div>
        <div id="nonimages">
            <div id="contentheading">
                <h1><?= $nemovitost['nazev'] ?></h1>
                <div class="clickables">
                    <a href="selection.php?mesto=<?= $nemovitost['mesto'] ?>"
                       class="city"><?= $mesta[$nemovitost['mesto']] ?> </a>
                    <a href="selection.php?kategorie=<?= $nemovitost['kategorie'] ?>"
                       class="category"><?= $kategorie[$nemovitost['kategorie']] ?></a>
                </div>
            </div>
            <div id="subheader">
                <div id="location"><?= $nemovitost['misto'] ?></div>
                <div id="price"><?= $nemovitost['cena'] ?></div>
            </div>
            <div id="centerinfo">
                <div id="description"><?= $nemovitost['popis'] ?></div>
                <table>
                    <?php foreach ($nemovitost['info'] as $key => $info): ?>
                        <tr>
                            <td><?= $key ?></td>
                            <td><?= $info ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>