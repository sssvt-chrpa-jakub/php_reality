<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nalezené inzeráty</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<?php
require('data.php');
include 'data.php';

function get_ads_for_type($array)
{
    global $_GET;
    $type = array_keys($_GET)[0];
    $typeID = (int)$_GET[$type][0];
    $adsForType = [];
    $index = 0;
    foreach ($array as $ad) {
        if ($ad[$type] === $typeID) {
            $adsForType[$index] = $ad;
            $index++;
        }
    }
    return $adsForType;
}

function get_header($array)
{
    global $mesta, $kategorie;
    $accessArray = ['mesto' => $mesta, 'kategorie' => $kategorie];
    return $accessArray[array_keys($array)[0]][$array[array_keys($array)[0]]];
}

?>

<header>
    <h1></h1>
    <a href="index.php"><img src="images/home.jpg" alt=""></a>
    <a href="index.php"><h1>SSSVT reality</h1></a>
    <div id="cities">
        <?php
        foreach ($mesta as $index => $mesto):
            ?>
            <a href="selection.php?mesto=<?= $index ?>"><?= $mesto ?></a>
        <?php endforeach; ?>
    </div>
    <div id="categories">
        <?php
        foreach ($kategorie as $index => $katka):
            ?>
            <a href="selection.php?kategorie=<?= $index ?>"><?= $katka ?></a>
        <?php endforeach; ?>
    </div>
    <div id="searchform">
        <form method="get" action="search.php">
            <input type="text" name="searchstring" value="" />
            <input type="submit" value="Hledat" />
        </form>
    </div>
</header>

<div class="content">
    <div id="contentheader">
        <h2>Nalezené inzeráty</h2>
        <h2><?= get_header($_GET) ?></h2>
    </div>
    <?php
    if (count(array_keys($_GET)) > 0):
        foreach (get_ads_for_type($nemovitosti) as $nemovitost):
            ?>
            <div class="nemovitost">
                <div class="nemovitostheader">
                    <a href="detail.php?id=<?=array_search($nemovitost,$nemovitosti)?>"><h1><?= $nemovitost['nazev'] ?></h1></a>
                    <div class="clickables">
                        <a href="selection.php?mesto=<?= $nemovitost['mesto'] ?>"
                           class="city"><?= $mesta[$nemovitost['mesto']] ?> </a>
                        <a href="selection.php?kategorie=<?= $nemovitost['kategorie'] ?>"
                           class="category"><?= $kategorie[$nemovitost['kategorie']] ?></a>
                    </div>
                </div>
                <div class="descriptionline">
                    <div class="description"><?= $nemovitost['popis'] ?></div>
                    <div class="foto"><img src="<?= $nemovitost['obrazky'][0] ?>" alt=""></div>
                </div>
                <div class="price"><?= $nemovitost['cena'] ?></div>
            </div>
        <?php endforeach; ?>

    <?php endif; ?>
</div>

</body>
</html>
