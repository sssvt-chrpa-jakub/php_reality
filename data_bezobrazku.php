<?php

$kategorie = array(
    0 => 'Byty k prodeji',
    1 => 'Byty k pronájmu',
    2 => 'Domy k prodeji',
    3 => 'Pozemky a zahrady'
);

$mesta = array(
    0 => 'Praha',
    1 => 'Brno',
    2 => 'Benešov',
    3 => 'Pardubice',
    4 => 'Hradec Králové',
    5 => 'Liberec',
    6 => 'Plzeň',
    7 => 'České budějovice'
);

$nemovitosti = array(
    array(
        'nazev' => 'Novostavba domu 6+1, 250 m?, pozemek 900 m?, Praha 6',
        'misto' => 'Praha 6, Dejvice, V Šáreckém údolí',
        'kategorie' => 2,
        'mesto' => 0,
        'popis' => 'Novostavba domu 6+1, 242 m?, pozemek 900 m?, Praha 6 Nebušice. Jedná se o novostavbu domu v krásném místě nedaleko přírodního parku Divoká Šárka. Dům je podsklepený a vybavený dvěmi garážemi. V každém patře je umístěna koupelna, v přízemí vybavena výřivou vanou, v prvním a suterenním prostoru sprcha a toalety zvlášť. Jedná se o slunný dům. Na podlahách dřevěné podlahy a dlažby. Energetická náročnost budovy G (nebyla sdělena vlastníkem nemovitosti).',
        'cena' => '19 900 000 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/7925000/10/7925012_1_orig.jpg?1480116654',
            'http://sta-reality.1gr.cz/img_upload/nemo/7925000/10/7925012_2_orig.jpg?1480116655',
            'http://sta-reality.1gr.cz/img_upload/nemo/7925000/10/7925012_3_orig.jpg?1480116655'
        ),
        'info' => array(
            'Počet podlaží budovy' => 3,
            'Zastavěná plocha' => '130', //m2
            'Stav budovy' => 'novostavba',
            'Vybavenost' => 'nezařízeno'
        )
    ),
    array(
        'nazev' => 'Prodej bytu 2+kk, 68 m?/B/sklep, OV, cihla, novostavba, Praha 10 - Uhříněves, ul. Václava Trojana',
        'misto' => 'Praha 10, Uhříněves, Václava Trojana',
        'kategorie' => 0,
        'mesto' => 0,
        'popis' => 'Nabízíme Vám pěkný zrekonstruovaný byt o dispozici 2+kk o ploše 86 m? s balkonem a sklepem v novostavbě v klidné lokalitě Prahy 10 - Uhříněves, ul. Václava Trojana, v osobním vlastnictví. Byt se nachází ve 3. patře cihlového domu s výtahem. Je vybaven novou nepoužívanou kuchyňskou linkou. Plastová okna. Dobrá občanská vybavenost - MŠ, ZŠ, obchody, pošta, dětská hřiště, sportoviště. Dobrá dopravní dostupnost - bus (depo Hostivař do 20 min. ). PENB "G" - štítek není k dispozici. V případě zájmu o koupi nemovitosti Vám zajistíme poradce v oblasti hypotečních úvěrů a stavebních spoření, a odborníka v oblasti oceňování nemovitostí a vyhotovení znaleckého posudku.',
        'cena' => '3 450 000 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8191700/70/8191771_1_orig.jpg?1481616151'
        ),
        'info' => array(
            'Poschodí' => 3,
            'Užitná plocha' => '68', //m2
            'Balkon' => 'ano',
            'Dopravní dostupnost' => 'MHD'
        )
    ),
    array(
        'nazev' => 'Pronájem bytu 1+1, 37 m?, Praha 3 - Vinohrady, metro A - Flora, volný ihned',
        'misto' => 'Praha 3, Vinohrady, Hradecká',
        'kategorie' => 1,
        'mesto' => 0,
        'popis' => 'Nabízíme k pronájmu velmi pěkný nezařízený byt 1+1, 37 m?, v přízemí udržovaného cihlového domu, v Praze 3 - Vinohrady, ul. Hradecká. Jedná se o kuchyň s kuchyňkou linkou a spotřebiči, pokoj s vestavěným patrem na spaní a koupelnu s vanou a toaletou. Dům se nachází ve velmi žádané lokalitě Prahy s perfektní dostupností do centra, v těsné blízkosti je stanice metra A - Flora. Byt je vhodný pro 1 - 2 osoby a k nastěhování je ihned. ID nabídky: N18812 ',
        'cena' => '11 999 Kč',
        'poplatky' => '2 100 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8188500/90/8188595_1_orig.jpg?1481126243',
            'http://sta-reality.1gr.cz/img_upload/nemo/8188500/90/8188595_2_orig.jpg?1481126243',
            'http://sta-reality.1gr.cz/img_upload/nemo/8188500/90/8188595_3_orig.jpg?1481126244'
        ),
        'info' => array(
            'Poschodí' => 1,
            'Užitná plocha' => '37', //m2
            'Vodní zdroj' => 'veřejný',
            'Vybavenost' => 'nezařízeno'
        )
    ),
    array(
        'nazev' => 'Prodej, pole, 89.796 m?, Česká Čermná',
        'misto' => 'Česká Čermná, okres Náchod',
        'kategorie' => 3,
        'mesto' => 4,
        'popis' => 'Nabízíme k prodeji pozemky v obci Česká Čermná v Orlických horách. Jedná se o prodej podílu o velikosti 1/2 na pozemcích o ploše 89.796 m?, z toho je 6.477 m? vedených jako lesní pozemky, zbylé jako trvalý travní porost, nebo ostatní plocha. Většina pozemků je dotčena pachtovní smlouvou na dobu neurčitou, s výpovědní lhůtou 6 let. Čísla pozemků budou zaslána na vyžádání, více informací podá makléř. ',
        'cena' => 'na vyžádání',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_1_orig.jpg?1481561122',
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_2_orig.jpg?1481561122',
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_3_orig.jpg?1481561123',
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_4_orig.jpg?1481561123'
        ),
        'info' => array(
            'Plocha parcely' => 89796 //m2
        )
    ),
    array(
        'nazev' => 'Novostavba RD 4+1, 100 m?, pozemek 348 m?',
        'misto' => 'Bučovice, náměstí Svobody',
        'kategorie' => 2,
        'mesto' => 1,
        'popis' => 'Naše společnost se zabývá komplexním zajištěním individuální výstavby RD, a to jak podle typových, tak i individuálních projektů, včetně zajištění vhodného pozemku pro stavbu. Typové domy lze na přání klienta upravovat. Služby jsou od výběru pozemku a domu, přes zajištění financování, předprojektovou a projektovou přípravu a zajištění výstavby, až po kolaudaci. Z našeho portfolia pro Vás vybíráme: Klasicky ztvárněný menší rodinný dům s dispozicí 4+kk. Vyhoví však i čtyřčlenné rodině. V přízemí se nachází denní část spojující obývací prostory s jídelním koutem a kuchyní. Zajímavým architektonickým prvkem je apsida rozšiřující - i přes menší zastavěnou plochu domu - obytnou přízemní část až na 41 m? a přes bohaté prosklení ústící na menší terasu. V přízemí je samostatné WC, oddělená technická místnost a zádveří s vestavěnou skříní. Obývák a 2 NP spojuje dřevěné interiérové schodiště. V patře jsou pak 3 samostatné pokoje (ložnice, dětský pokoj, pracovna/šatna) a větší koupelna (WC, rohová vana, 2 umyvadla). Z obou ložnic je výstup na menší balkon. Technologicky se jedná o klasickou zděnou stavbu (cihelné bloky P+D 25/30cm) se zateplenou fasádou. Krov je dřevěný s krytinou - betonová taška. Standard zahrnuje bílé otvorové výplně z pěti komorového profilu s izolačním trojsklem. Vytápění elektrokotel, v přízemí podlahové, v patře ocelové radiátory. Doba výstavby 10 měsíců, záruka na dílo 36 měsíců. Projekt pro stavební povolení a inženýring jsou v ceně. Bonus: Výkon stavebního dozoru po dobu výstavby zdarma. Tato nabídka je pouze inspirativní. Může být realizována v této podobě, nebo je možné naše domy a pozemky vzájemně kombinovat v mnoha různých variantách. Bližší informace získáte na uvedených kontaktech. ',
        'cena' => '3 200 934 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8080100/40/8080144_1_orig.jpg?1478090406',
            'http://sta-reality.1gr.cz/img_upload/nemo/8080100/40/8080144_2_orig.jpg?1478090407',
            'http://sta-reality.1gr.cz/img_upload/nemo/8080100/40/8080144_3_orig.jpg?1478090407'
        ),
        'info' => array(
            'Počet podlaží budovy' => 2,
            'Zastavěná plocha' => 62, //m2
            'Stav budovy' => 'novostavba',
            'Vybavenost' => 'nezařízeno',
            'Balkon' => 'balkon, terasa'
        )
    ),
    array(
        'nazev' => 'Prodej, byt 1+kk, Hradec Králové',
        'misto' => 'Nový Hradec Králové, Jana Masaryka',
        'kategorie' => 0,
        'mesto' => 4,
        'popis' => 'Nabízíme ke koupi bytovou jednotku o dispozici 1+kk s prostornou lodžií v oblíbené lokalitě Moravského předměstí v Hradci Králové. Byt se nachází ve 3. podlaží revitalizovaného bytového domu, který je díky 6 výtahům plně bezbariérový. Jednotka se prodává kompletně zařízená a náleží k ní také sklepní kóje. Doporučujeme jako investici díky nízkým nákladům na bydlení. Rádi pro Vás také zajistíme slušného nájemníka. Přilehlý park nabízí posezení a venkovní posilovací stroje. Výborná občanská vybavenost v lokalitě. Bezplatně pro Vás vyřídíme výhodné financování hypotečním úvěrem. ',
        'cena' => '1 390 000 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8179100/40/8179148_1_orig.jpg?1479976236',
            'http://sta-reality.1gr.cz/img_upload/nemo/8179100/40/8179148_2_orig.jpg?1479976236',
            'http://sta-reality.1gr.cz/img_upload/nemo/8179100/40/8179148_3_orig.jpg?1479976236'
        ),
        'info' => array(
            'Poschodí' => 3,
            'Užitná plocha' => '68', //m2
            'Balkon' => 'ano',
            'Dopravní dostupnost' => 'MHD'
        )
    ),
    
    
    
    array(
        'nazev' => 'Novostavba domu 6+1, 250 m?, pozemek 900 m?, Praha 6',
        'misto' => 'Praha 6, Dejvice, V Šáreckém údolí',
        'kategorie' => 2,
        'mesto' => 2,
        'popis' => 'Novostavba domu 6+1, 242 m?, pozemek 900 m?, Praha 6 Nebušice. Jedná se o novostavbu domu v krásném místě nedaleko přírodního parku Divoká Šárka. Dům je podsklepený a vybavený dvěmi garážemi. V každém patře je umístěna koupelna, v přízemí vybavena výřivou vanou, v prvním a suterenním prostoru sprcha a toalety zvlášť. Jedná se o slunný dům. Na podlahách dřevěné podlahy a dlažby. Energetická náročnost budovy G (nebyla sdělena vlastníkem nemovitosti).',
        'cena' => '19 900 000 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/7925000/10/7925012_1_orig.jpg?1480116654',
            'http://sta-reality.1gr.cz/img_upload/nemo/7925000/10/7925012_2_orig.jpg?1480116655',
            'http://sta-reality.1gr.cz/img_upload/nemo/7925000/10/7925012_3_orig.jpg?1480116655'
        ),
        'info' => array(
            'Počet podlaží budovy' => 3,
            'Zastavěná plocha' => '130', //m2
            'Stav budovy' => 'novostavba',
            'Vybavenost' => 'nezařízeno'
        )
    ),
    array(
        'nazev' => 'Prodej bytu 2+kk, 68 m?/B/sklep, OV, cihla, novostavba, Praha 10 - Uhříněves, ul. Václava Trojana',
        'misto' => 'Praha 10, Uhříněves, Václava Trojana',
        'kategorie' => 0,
        'mesto' => 2,
        'popis' => 'Nabízíme Vám pěkný zrekonstruovaný byt o dispozici 2+kk o ploše 86 m? s balkonem a sklepem v novostavbě v klidné lokalitě Prahy 10 - Uhříněves, ul. Václava Trojana, v osobním vlastnictví. Byt se nachází ve 3. patře cihlového domu s výtahem. Je vybaven novou nepoužívanou kuchyňskou linkou. Plastová okna. Dobrá občanská vybavenost - MŠ, ZŠ, obchody, pošta, dětská hřiště, sportoviště. Dobrá dopravní dostupnost - bus (depo Hostivař do 20 min. ). PENB "G" - štítek není k dispozici. V případě zájmu o koupi nemovitosti Vám zajistíme poradce v oblasti hypotečních úvěrů a stavebních spoření, a odborníka v oblasti oceňování nemovitostí a vyhotovení znaleckého posudku.',
        'cena' => '3 450 000 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8191700/70/8191771_1_orig.jpg?1481616151'
        ),
        'info' => array(
            'Poschodí' => 3,
            'Užitná plocha' => '68', //m2
            'Balkon' => 'ano',
            'Dopravní dostupnost' => 'MHD'
        )
    ),
    array(
        'nazev' => 'Pronájem bytu 1+1, 37 m?, Praha 3 - Vinohrady, metro A - Flora, volný ihned',
        'misto' => 'Praha 3, Vinohrady, Hradecká',
        'kategorie' => 1,
        'mesto' => 2,
        'popis' => 'Nabízíme k pronájmu velmi pěkný nezařízený byt 1+1, 37 m?, v přízemí udržovaného cihlového domu, v Praze 3 - Vinohrady, ul. Hradecká. Jedná se o kuchyň s kuchyňkou linkou a spotřebiči, pokoj s vestavěným patrem na spaní a koupelnu s vanou a toaletou. Dům se nachází ve velmi žádané lokalitě Prahy s perfektní dostupností do centra, v těsné blízkosti je stanice metra A - Flora. Byt je vhodný pro 1 - 2 osoby a k nastěhování je ihned. ID nabídky: N18812 ',
        'cena' => '11 999 Kč',
        'poplatky' => '2 100 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8188500/90/8188595_1_orig.jpg?1481126243',
            'http://sta-reality.1gr.cz/img_upload/nemo/8188500/90/8188595_2_orig.jpg?1481126243',
            'http://sta-reality.1gr.cz/img_upload/nemo/8188500/90/8188595_3_orig.jpg?1481126244'
        ),
        'info' => array(
            'Poschodí' => 1,
            'Užitná plocha' => '37', //m2
            'Vodní zdroj' => 'veřejný',
            'Vybavenost' => 'nezařízeno'
        )
    ),
    array(
        'nazev' => 'Prodej, pole, 89.796 m?, Česká Čermná',
        'misto' => 'České Budějovice',
        'kategorie' => 3,
        'mesto' => 7,
        'popis' => 'Nabízíme k prodeji pozemky v obci Česká Čermná v Orlických horách. Jedná se o prodej podílu o velikosti 1/2 na pozemcích o ploše 89.796 m?, z toho je 6.477 m? vedených jako lesní pozemky, zbylé jako trvalý travní porost, nebo ostatní plocha. Většina pozemků je dotčena pachtovní smlouvou na dobu neurčitou, s výpovědní lhůtou 6 let. Čísla pozemků budou zaslána na vyžádání, více informací podá makléř. ',
        'cena' => 'na vyžádání',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_1_orig.jpg?1481561122',
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_2_orig.jpg?1481561122',
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_3_orig.jpg?1481561123',
            'http://sta-reality.1gr.cz/img_upload/nemo/8191500/90/8191599_4_orig.jpg?1481561123'
        ),
        'info' => array(
            'Plocha parcely' => 89796 //m2
        )
    ),
    array(
        'nazev' => 'Novostavba RD 4+1, 100 m?, pozemek 348 m?',
        'misto' => 'Liberec, náměstí Svobody',
        'kategorie' => 2,
        'mesto' => 5,
        'popis' => 'Naše společnost se zabývá komplexním zajištěním individuální výstavby RD, a to jak podle typových, tak i individuálních projektů, včetně zajištění vhodného pozemku pro stavbu. Typové domy lze na přání klienta upravovat. Služby jsou od výběru pozemku a domu, přes zajištění financování, předprojektovou a projektovou přípravu a zajištění výstavby, až po kolaudaci. Z našeho portfolia pro Vás vybíráme: Klasicky ztvárněný menší rodinný dům s dispozicí 4+kk. Vyhoví však i čtyřčlenné rodině. V přízemí se nachází denní část spojující obývací prostory s jídelním koutem a kuchyní. Zajímavým architektonickým prvkem je apsida rozšiřující - i přes menší zastavěnou plochu domu - obytnou přízemní část až na 41 m? a přes bohaté prosklení ústící na menší terasu. V přízemí je samostatné WC, oddělená technická místnost a zádveří s vestavěnou skříní. Obývák a 2 NP spojuje dřevěné interiérové schodiště. V patře jsou pak 3 samostatné pokoje (ložnice, dětský pokoj, pracovna/šatna) a větší koupelna (WC, rohová vana, 2 umyvadla). Z obou ložnic je výstup na menší balkon. Technologicky se jedná o klasickou zděnou stavbu (cihelné bloky P+D 25/30cm) se zateplenou fasádou. Krov je dřevěný s krytinou - betonová taška. Standard zahrnuje bílé otvorové výplně z pěti komorového profilu s izolačním trojsklem. Vytápění elektrokotel, v přízemí podlahové, v patře ocelové radiátory. Doba výstavby 10 měsíců, záruka na dílo 36 měsíců. Projekt pro stavební povolení a inženýring jsou v ceně. Bonus: Výkon stavebního dozoru po dobu výstavby zdarma. Tato nabídka je pouze inspirativní. Může být realizována v této podobě, nebo je možné naše domy a pozemky vzájemně kombinovat v mnoha různých variantách. Bližší informace získáte na uvedených kontaktech. ',
        'cena' => '3 200 934 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8080100/40/8080144_1_orig.jpg?1478090406',
            'http://sta-reality.1gr.cz/img_upload/nemo/8080100/40/8080144_2_orig.jpg?1478090407',
            'http://sta-reality.1gr.cz/img_upload/nemo/8080100/40/8080144_3_orig.jpg?1478090407'
        ),
        'info' => array(
            'Počet podlaží budovy' => 2,
            'Zastavěná plocha' => 62, //m2
            'Stav budovy' => 'novostavba',
            'Vybavenost' => 'nezařízeno',
            'Balkon' => 'balkon, terasa'
        )
    ),
    array(
        'nazev' => 'Prodej, byt 1+kk, Plzeň',
        'misto' => 'Nová Plzeň, Jana Masaryka',
        'kategorie' => 0,
        'mesto' => 6,
        'popis' => 'Nabízíme ke koupi bytovou jednotku o dispozici 1+kk s prostornou lodžií v oblíbené lokalitě Moravského předměstí v Hradci Králové. Byt se nachází ve 3. podlaží revitalizovaného bytového domu, který je díky 6 výtahům plně bezbariérový. Jednotka se prodává kompletně zařízená a náleží k ní také sklepní kóje. Doporučujeme jako investici díky nízkým nákladům na bydlení. Rádi pro Vás také zajistíme slušného nájemníka. Přilehlý park nabízí posezení a venkovní posilovací stroje. Výborná občanská vybavenost v lokalitě. Bezplatně pro Vás vyřídíme výhodné financování hypotečním úvěrem. ',
        'cena' => '1 390 000 Kč',
        'obrazky' => array(
            'http://sta-reality.1gr.cz/img_upload/nemo/8179100/40/8179148_1_orig.jpg?1479976236',
            'http://sta-reality.1gr.cz/img_upload/nemo/8179100/40/8179148_2_orig.jpg?1479976236',
            'http://sta-reality.1gr.cz/img_upload/nemo/8179100/40/8179148_3_orig.jpg?1479976236'
        ),
        'info' => array(
            'Poschodí' => 3,
            'Užitná plocha' => '68', //m2
            'Balkon' => 'ano',
            'Dopravní dostupnost' => 'MHD'
        )
    ),
);