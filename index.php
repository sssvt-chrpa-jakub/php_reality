<!doctype html>
<html lang="en">
<head>
    <link rel="stylesheet" href="style.css">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reality</title>
</head>
<body>
<?php
require('data.php');
include 'data.php';

function select_rand_ads($array, $amount)
{
    $numbers = range(0, count($array) - 1);
    shuffle($numbers);
    $randomAds = [];
    foreach (array_slice($numbers, 0, $amount) as $key => $index) {
        $randomAds[$key] = $array[$index];
    }
    return $randomAds;
}

?>
<header>
    <a href="index.php"><img src="images/home.jpg" alt=""></a>
    <a href="index.php"><h1>SSSVT reality</h1></a>

    <div id="cities">
        <?php
        foreach ($mesta as $index => $mesto):
            ?>
            <a href="selection.php?mesto=<?= $index ?>"><?= $mesto ?></a>
        <?php endforeach; ?>
    </div>
    <div id="categories">
        <?php
        foreach ($kategorie as $index => $katka):
            ?>
            <a href="selection.php?kategorie=<?= $index ?>"><?= $katka ?></a>
        <?php endforeach; ?>
    </div>
    <div id="searchform">
    <form method="get" action="search.php">
        <input type="text" name="searchstring" value=""/>
        <input type="submit" value="Hledat" />
    </form>
    </div>

</header>


<div class="content">
    <h2>Výběr z inzerátů</h2>
    <div class="inzeraty">
        <?php
        foreach (select_rand_ads($nemovitosti, 3) as $nemovitost):
            ?>
            <div class="nemovitost">
                <div class="nemovitostheader">
                    <a href="detail.php?id=<?= array_search($nemovitost, $nemovitosti) ?>">
                        <h1><?= $nemovitost['nazev'] ?></h1></a>
                    <div class="clickables">
                        <a href="selection.php?mesto=<?= $nemovitost['mesto'] ?>"
                           class="city"><?= $mesta[$nemovitost['mesto']] ?> </a>
                        <a href="selection.php?kategorie=<?= $nemovitost['kategorie'] ?>"
                           class="category"><?= $kategorie[$nemovitost['kategorie']] ?></a>
                    </div>
                </div>
                <div id="location"><?= $nemovitost['misto'] ?></div>
                <div class="descriptionline">
                    <div class="description"><?= $nemovitost['popis'] ?></div>
                    <div class="foto"><img src="<?= $nemovitost['obrazky'][0] ?>" alt=""></div>
                </div>
                <div class="price"><?= $nemovitost['cena'] ?></div>
            </div>
        <?php endforeach; ?>
    </div>

</div>

</body>
</html>